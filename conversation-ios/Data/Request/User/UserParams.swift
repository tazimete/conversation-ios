//
//  MovieRequestParameter.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation
import Foundation


struct UserParams: Parameterizable{
    let apiKey: String = AppConfig.shared.getServerConfig().setAuthCredential().apiKey ?? ""
    let limit: Int

    public init(limit: Int) {
        self.limit = limit
    }

    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case limit = "results"
    }

    public var asRequestParam: [String: Any] {
        let param: [String: Any] = [CodingKeys.apiKey.rawValue: apiKey, CodingKeys.limit.rawValue: limit]
        return param.compactMapValues { $0 }
    }
}
