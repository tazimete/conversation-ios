//
//  ConversationRequestParams.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation

struct ConversationRequestParams: Parameterizable{
    let apiKey: String = AppConfig.shared.getServerConfig().setAuthCredential().apiKey ?? ""
    let limit: Int

    public init(limit: Int) {
        self.limit = limit
    }

    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case limit = "results"
    }

    public var asRequestParam: [String: Any] {
        let param: [String: Any] = [CodingKeys.apiKey.rawValue: apiKey, CodingKeys.limit.rawValue: limit]
        return param.compactMapValues { $0 }
    }
}
