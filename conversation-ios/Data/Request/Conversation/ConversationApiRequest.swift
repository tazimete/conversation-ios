//
//  ConversationApiRequest.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation


enum ConversationApiRequest {
    case sendMessage(params: Parameterizable)
    case getMessage(params: Parameterizable)
}

extension ConversationApiRequest: APIRequest {
    public var baseURL: URL {
        let url =  "\(AppConfig.shared.getServerConfig().getBaseUrl())/\(AppConfig.shared.getServerConfig().getApiVersion())/"
        return URL(string: url)!
    }
    
    public typealias ItemType = Message
    public typealias ResponseType = ItemType
    
    public var method: RequestType {
        switch self {
            case .sendMessage: return .POST
            case .getMessage: return .GET
        }
    }
    
    public var path: String {
        switch self {
            case .sendMessage: return ""
            case .getMessage: return ""
        }
    }
    
    public var parameters: [String: Any]{
        var parameter: [String: Any] = [:]
        
        switch self {
            case .sendMessage (let params):
                parameter = params.asRequestParam
            case .getMessage (let params):
                parameter = params.asRequestParam
        }
        
        return parameter
    }
    
    public var headers: [String: String] {
        return [String: String]()
    }
}


