//
//  MovieUsecase.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation
import RxSwift

/* This is User usecase class implementation from AbstractUserUsecase. Which will be used to get user related data from user repository*/
class UserUsecase: AbstractUserUsecase {
    var repository: AbstractRepository
    
    public init(repository: AbstractUserRepository) {
        self.repository = repository
    }
    
    func getUserList(limit: Int) -> Observable<UserApiRequest.ResponseType> {
        return (repository as! AbstractUserRepository).get(limit: limit)
    }
}
