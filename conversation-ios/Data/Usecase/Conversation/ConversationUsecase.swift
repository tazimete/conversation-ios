//
//  ConversationUsecase.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation
import RxSwift

/* This is Conversation usecase class implementation from AbstractConversationUsecase. Which will be used to get conversation related data from conversation repository*/
class ConversationUsecase: AbstractConversationUsecase {
    var repository: AbstractRepository
    
    public init(repository: AbstractConversationRepository) {
        self.repository = repository
    }

    func postMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
        return (repository as! AbstractConversationRepository).get(message: message)
    }
    
    func getMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
        return (repository as! AbstractConversationRepository).post(message: message)
    }
}
