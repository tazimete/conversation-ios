//
//  ConversationRepository.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation
import RxSwift
import UIKit

/* This is Conversation repository class implementation from AbstractUserRepository. Which will be used to get conversation related from api client/server response*/
class ConversationRepository: AbstractConversationRepository {
    var apiClient: AbstractApiClient
    
    init(apiClient: AbstractApiClient = APIClient.shared) {
        self.apiClient = apiClient
    }
    
    public func post(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
//        return apiClient.send(apiRequest: UserApiRequest.getUser(params: UserParams(limit: limit)), type: UserApiRequest.ResponseType.self)
        //post message to server 
        return get(message: message)
    }
    
    public func get(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
//        return apiClient.send(apiRequest: UserApiRequest.getUser(params: UserParams(limit: limit)), type: UserApiRequest.ResponseType.self)
        return Observable.create { [weak self] observer -> Disposable in
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
                observer.onNext( Message(id: message.id, userToId: message.userFromId, userFromId: message.userToId, userToName: message.userFromName, userFromName: message.userToName, body: message.body, isIncoming: !(message.isIncoming ?? false)))
                observer.onCompleted()
            })
        
            return Disposables.create()
        }
    }
}
