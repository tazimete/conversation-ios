//
//  UserRepository.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/11/21.
//

import Foundation
import RxSwift
import UIKit

/* This is User repository class implementation from AbstractUserRepository. Which will be used to get user related from api client/server response*/
class UserRepository: AbstractUserRepository {
    var apiClient: AbstractApiClient
    
    init(apiClient: AbstractApiClient = APIClient.shared) {
        self.apiClient = apiClient
    }
    
    public func get(limit: Int) -> Observable<UserApiRequest.ResponseType> {
//        return apiClient.send(apiRequest: UserApiRequest.getUser(params: UserParams(limit: limit)), type: UserApiRequest.ResponseType.self)
        return Observable.create { [weak self] observer -> Disposable in
            DispatchQueue.main.asyncAfter(deadline: .now()+2.0, execute: {
                let response = [
                    User(id: "100", name: "James Feelings", gender: "Male", email: "james@feelings.com", message: "Hi Everyone...", picture: "James"),
                    User(id: "101", name: "Hasan ARK", gender: "Male", email: "hasan@ark.com", message: "Good morning ...", picture: "Hasan"),
                    User(id: "102", name: "Ayub LRB", gender: "Male", email: "ayub@lrb.com", message: "Hello Legends...", picture: "Ayub"),
                    User(id: "103", name: "Biplob Prometheus", gender: "Male", email: "biplob@prometheus.com", message: "Good bye", picture: "Biplob"),
                    User(id: "104", name: "Shafin A", gender: "Male", email: "shafin@miles.com", message: "How are you?", picture: "Shafin"),
                    User(id: "105", name: "Habib Wahid", gender: "Male", email: "habib@hw.com", message: "I am fin", picture: "Habib"),
                    User(id: "106", name: "Hridoy Khan", gender: "Male", email: "hridoy@hk.com", message: "Hello Dhaka", picture: "Hridoy"),
                    User(id: "107", name: "Azam Khan", gender: "Male", email: "azam@uk.com", message: "Good night", picture: "Azam"),
                    User(id: "108", name: "Mizan Warefaze", gender: "Male", email: "mizan@warefaze.com", message: "Ok bye", picture: "Mizan"),
                    User(id: "100", name: "D Costa Artcell", gender: "Male", email: "dcosta@artcell.com", message: "Long time later", picture: "D Costa"),
                    User(id: "109", name: "Asif", gender: "Male", email: "asif@uk.com", message: "Sabash bangladesh", picture: "Asif"),
                    User(id: "110", name: "Shubir nondi", gender: "Male", email: "shubir@uk.com", message: "Hello everyone", picture: "shubir")
                ]
                
                observer.onNext( Response<User>(results: Array(response.prefix(upTo: limit))))
                observer.onCompleted()
            })
        
            return Disposables.create()
        }
    }
}
