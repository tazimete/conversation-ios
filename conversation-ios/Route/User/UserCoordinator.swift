//
//  RootCoordinator.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 31/7/21.
//

import UIKit

class UserCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    public func start() -> MyFriendsViewController{
        let repository = UserRepository(apiClient: APIClient.shared)
        let usecase = UserUsecase(repository: repository)
        let viewModel = UserViewModel(usecase: usecase)
        let vc = MyFriendsViewController(viewModel: viewModel)
        self.navigationController.pushViewController(vc, animated: true)
        
        return vc
    }
    
    public func showConversationController(with friend: User) -> ConversationViewController{
        let repository = ConversationRepository(apiClient: APIClient.shared)
        let usecase = ConversationUsecase(repository: repository)
        let viewModel = ConversationViewModel(usecase: usecase)
        let vc = ConversationViewController.instantiate(viewModel: viewModel)
        vc.friend = friend
        self.navigationController.pushViewController(vc, animated: true)
        
        return vc
    }
}
