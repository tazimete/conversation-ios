//
//  ConversationViewModel.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

/* This is Conversation viewmodel implementation  of AbstractConversationViewModel. Which will be used to get conversation related data by conversation usecase*/
class ConversationViewModel: AbstractConversationViewModel {
    
    // This struct will be used get event with data from viewcontroller
    public struct ConversationInput {
        let trigger: Observable<Message>
    }
    
    // This struct will be used send event with observable data/response to viewcontroller
    public struct ConversationOutput {
        let messageList: BehaviorRelay<[ConversationApiRequest.ResponseType]>
        let errorTracker: BehaviorRelay<NetworkError?>
    }
    
    public var usecase: AbstractUsecase
    
    public init(usecase: AbstractConversationUsecase) {
        self.usecase = usecase
    }
    
    public func getConversationOutput(input: ConversationInput) -> ConversationOutput {
        var conversationResponse = BehaviorRelay<[ConversationApiRequest.ItemType]>(value: [])
        let errorResponse = BehaviorRelay<NetworkError?>(value: nil)
        
        input.trigger.flatMapLatest({ [weak self] (inputModel) -> Observable<ConversationApiRequest.ResponseType> in
            guard let weakSelf = self else {
                return Observable.just(ConversationApiRequest.ResponseType())
            }
            
            //show shimmer
            conversationResponse.accept(conversationResponse.value + [inputModel])
            
            //fetch user list
            return weakSelf.getMessage(message: inputModel)
                .catch({ error in
                    errorResponse.accept(error as? NetworkError)
                    
                    return Observable.just(ConversationApiRequest.ResponseType())
                })
        }).subscribe(onNext: {
            response in
            conversationResponse.accept(conversationResponse.value + [response])
        })

        return ConversationOutput.init(messageList: conversationResponse, errorTracker: errorResponse)
    }
    
    func getMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
        return (usecase as! AbstractConversationUsecase).getMessage(message: message)
    }
    
    func sendMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType> {
        return (usecase as! AbstractConversationUsecase).postMessage(message: message)
    }
}
