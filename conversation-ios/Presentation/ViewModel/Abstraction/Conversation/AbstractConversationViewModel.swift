//
//  AbstractConversationViewModel.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation

import RxSwift

/* This is Conversation viewmodel abstraction extented from AbstractViewModel. Which will be used to get conversation related data by conversation usecase*/
protocol AbstractConversationViewModel: AbstractViewModel {
    // Transform the conversation list input to output observable
    func getConversationOutput(input: ConversationViewModel.ConversationInput) -> ConversationViewModel.ConversationOutput
    
    // get conversation data through api call
    func getMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType>
    
    // get conversation data through api call
    func sendMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType>
}
