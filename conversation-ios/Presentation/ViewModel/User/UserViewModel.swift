//
//  UserViewModel.swift
//  randomUser-ios
//
//  Created by AGM Tazim on 1/11/21.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

/* This is User viewmodel implementation  of AbstractUserViewModel. Which will be used to get user related data by user usecase*/
class UserViewModel: AbstractUserViewModel {
    
    // This struct will be used get event with data from viewcontroller
    public struct UserListInput {
        let trigger: Observable<UserListInputModel>
    }
    
    // This struct will be used send event with observable data/response to viewcontroller
    public struct UserListOutput {
        let userList: BehaviorRelay<[UserApiRequest.ItemType]>
        let errorTracker: BehaviorRelay<NetworkError?>
    }
    
    // This struct will be used get input from viewcontroller
    public struct UserListInputModel {
        let limit: Int
    }
    
    public var usecase: AbstractUsecase
    
    public init(usecase: AbstractUserUsecase) {
        self.usecase = usecase
    }
    
    public func getUserListOutput(input: UserListInput) -> UserListOutput {
        let userListResponse = BehaviorRelay<[UserApiRequest.ItemType]>(value: [])
        let errorResponse = BehaviorRelay<NetworkError?>(value: nil)
        
        input.trigger.flatMapLatest({ [weak self] (inputModel) -> Observable<UserApiRequest.ResponseType> in
            guard let weakSelf = self else {
                return Observable.just(UserApiRequest.ResponseType())
            }
            
            debugPrint("UserViewModel -- getUserListOutput() -- trigger -- limit = \(inputModel.limit)")
            
            //show shimmer
            userListResponse.accept(Array<UserApiRequest.ItemType>(repeating: UserApiRequest.ItemType(), count: 9))
            
            //fetch user list
            return weakSelf.getUserList(limit: inputModel.limit)
                .catch({ error in
                    errorResponse.accept(error as? NetworkError)
                    
                    return Observable.just(UserApiRequest.ResponseType())
                })
        }).subscribe(onNext: {
            response in
            userListResponse.accept(response.results ?? [])
        })

        return UserListOutput.init(userList: userListResponse, errorTracker: errorResponse)
    }
    
    func getUserList(limit: Int) -> Observable<UserApiRequest.ResponseType> {
        return (usecase as! AbstractUserUsecase).getUserList(limit: limit)
    }
}
