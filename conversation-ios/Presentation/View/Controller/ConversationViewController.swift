//
//  ConversationViewController.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/11/21.
//

import UIKit
import RxSwift
import RxCocoa

class ConversationViewController: BaseViewController {
    // MARK: Non UI Objects
    public var conversationViewModel: AbstractConversationViewModel!
    private let disposeBag = DisposeBag()
    private let conversationTrigger = PublishSubject<ConversationApiRequest.ResponseType>()
    public let messageSubject = PublishSubject<Message>()
    public var friend: User!
    
    // MARK: UI Objects
    @IBOutlet weak var tvMessageList: UITableView!
    @IBOutlet weak var uivMessageInputView: UIView!
    @IBOutlet weak var tfMessageInput: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func initView() {
        super.initView()
        
        //config tableview
        tvMessageList.separatorInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tvMessageList.separatorStyle = .none
        tvMessageList.allowsSelection = false
        tvMessageList.showsHorizontalScrollIndicator = false
        tvMessageList.showsVerticalScrollIndicator = false
        
        //cell registration
        tvMessageList.register(UINib(nibName: IncommingMessageCellConfig.reuseId, bundle: nil), forCellReuseIdentifier: IncommingMessageCellConfig.reuseId)
        tvMessageList.register(UINib(nibName: OutgoingMessageCellConfig.reuseId, bundle: nil), forCellReuseIdentifier: OutgoingMessageCellConfig.reuseId)
        tvMessageList.rx.setDelegate(self).disposed(by: disposeBag)
    }
    
    override func initNavigationBar() {
        self.navigationItem.title = "Conversation"
    }
    
    // MARK: Actions
    @IBAction func didTapDoneButton(_ sender: UIButton) {
        let messageBody = tfMessageInput.text
        tfMessageInput.text = ""
        
        if !(messageBody?.isEmpty ?? false) {
            let message = Message(id: "1", userToId: friend.id, userFromId: "100", userToName: friend.name, userFromName: "Me", body: messageBody, isIncoming: false)
            conversationTrigger.onNext(message)
            messageSubject.onNext(message)
        }
    }
    
    override func bindViewModel() {
        conversationViewModel = (viewModel as! AbstractConversationViewModel)
        let input = ConversationViewModel.ConversationInput(trigger: conversationTrigger)
        let output = conversationViewModel.getConversationOutput(input: input)

        //populate table view
        output.messageList.observe(on: MainScheduler.instance).observe(on: MainScheduler.instance)
            .bind(to: tvMessageList.rx.items) { [weak self] tableView, row, model in
                guard let weakSelf = self else {
                    return UITableViewCell()
                }

                return weakSelf.populateTableViewCell(viewModel: model.asCellViewModel, indexPath: IndexPath(row: row, section: 0), tableView: tableView)
            }.disposed(by: disposeBag)

        // detect error
        output.errorTracker.observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in

                guard let weakSelf = self, let error = error else {
                    return
                }

            print("\(String(describing: weakSelf.TAG)) -- bindViewModel() -- error  -- code = \(error.errorCode), message = \(error.errorMessage)")
        }).disposed(by: disposeBag)
    }
    
    //populate table view cell
    private func populateTableViewCell(viewModel: MessageCellViewModel, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        var item: CellConfigurator = IncommingMessageCellConfig.init(item: viewModel)
        
        if !(viewModel.isIncoming ?? false) {
            item = OutgoingMessageCellConfig.init(item: viewModel)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
}
