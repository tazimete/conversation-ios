//
//  ViewController.swift
//  setScheduleTest
//
//  Created by JMC on 29/10/21.
//

import UIKit
import RxSwift
import RxCocoa

class MyFriendsViewController: BaseViewController {
    // MARK: Non UI Proeprties
    public var userViewModel: AbstractUserViewModel!
    private let disposeBag = DisposeBag()
    private let userListTrigger = PublishSubject<UserViewModel.UserListInputModel>()
    private var userListRelay: BehaviorRelay<[User]>!
    private var selectedIndexpath: IndexPath?
    private var rootCoordinator: Coordinator?
    
    // MARK: UI Proeprties
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.separatorInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        //cell registration
        tableView.register(UserItemCell.self, forCellReuseIdentifier: UserItemCellConfig.reuseId)
        tableView.register(UserShimmerCell.self, forCellReuseIdentifier: UserShimmerCellConfig.reuseId)
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        return tableView
    }()


    // MARK: Constructors
    init(viewModel: AbstractUserViewModel) {
        super.init(viewModel: viewModel)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        rootCoordinator = makeRootCoordinator()
    }
    
    public func makeRootCoordinator() -> Coordinator {
        return (view.window?.windowScene?.delegate as! SceneDelegate).rootCoordinator 
    }

    // MARK: Overrriden Methods
    override func initView() {
        super.initView()
        
        //setup tableview
        view.addSubview(tableView)
        
        //set anchor
        tableView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: view.frame.width, height: 0, enableInsets: true)
        
        //table view
        onTapTableviewCell()
    }
    
    //when theme change, we can also define dark mode color option in color asse
    override public func applyDarkTheme() {
        navigationController?.navigationBar.backgroundColor = .lightGray
        tableView.backgroundColor = .black
        self.navigationItem.rightBarButtonItem?.tintColor = .lightGray
        tableView.reloadData()
    }
    
    override public func applyNormalTheme() {
        navigationController?.navigationBar.backgroundColor = .white
        tableView.backgroundColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = .black
        tableView.reloadData()
    }
    
    override func initNavigationBar() {
        self.navigationItem.title = "My Friends"
    }
    
    override func bindViewModel() {
        userViewModel = (viewModel as! AbstractUserViewModel)
        let input = UserViewModel.UserListInput(trigger: userListTrigger)
        let output = userViewModel.getUserListOutput(input: input)

        //populate table view
        userListRelay = output.userList
        userListRelay.observe(on: MainScheduler.instance)
            .bind(to: tableView.rx.items) { [weak self] tableView, row, model in
                guard let weakSelf = self else {
                    return UITableViewCell()
                }

                return weakSelf.populateTableViewCell(viewModel: model.asCellViewModel, indexPath: IndexPath(row: row, section: 0), tableView: tableView)
            }.disposed(by: disposeBag)

        // detect error
        output.errorTracker.observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in

                guard let weakSelf = self, let error = error else {
                    return
                }

            print("\(String(describing: weakSelf.TAG)) -- bindViewModel() -- error  -- code = \(error.errorCode), message = \(error.errorMessage)")
        }).disposed(by: disposeBag)
        
        // fire user list trigger
        getUserList(limit: 5)
    }
    
    public func getUserList(limit: Int) {
        userListTrigger.onNext(UserViewModel.UserListInputModel(limit: limit))
    }
    
    private func navigateToConversationController(with friend: User) {
        //get last message from conversation view controller
        let vc = rootCoordinator?.showConversationController(with: friend)
        vc?.messageSubject.subscribe(onNext: { [weak self] message in
            guard let weakSelf = self else {
                return
            }
            
            if let indexPath = weakSelf.selectedIndexpath {
                var data = weakSelf.userListRelay.value
                data.remove(at: indexPath.row)
                weakSelf.userListRelay.accept([User(id: message.userToId, name: message.userToName, gender: "Male", email: "N/A", message: message.body, picture: message.userToName)] + data)
                weakSelf.selectedIndexpath = IndexPath(row: 0, section: 0)
            }
        }).disposed(by: disposeBag)
    }
    
    //populate table view cell
    private func populateTableViewCell(viewModel: AbstractCellViewModel, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        var item: CellConfigurator = UserShimmerCellConfig.init(item: viewModel)
        
        if viewModel.id != nil {
            item = UserItemCellConfig.init(item: viewModel)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
    
    // MARK: Actions
    private func onTapTableviewCell() {
        Observable
            .zip(tableView.rx.itemSelected, tableView.rx.modelSelected(User.self))
            .bind { [weak self] indexPath, model in
                guard let weakSelf = self else {
                    return
                }
                weakSelf.selectedIndexpath = indexPath
                weakSelf.tableView.deselectRow(at: indexPath, animated: true)
                print("\(weakSelf.TAG) -- onTapTableviewCell() -- Selected " + (model.name ?? "") + " at \(indexPath)")
                
                //navigate to profile view controller
                weakSelf.navigateToConversationController(with: model)
            }
            .disposed(by: disposeBag)
    }
}


