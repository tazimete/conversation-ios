//
//  UserCellViewModel.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 4/11/21.
//

import Foundation


protocol AbstractCellViewModel: AnyObject {
    var id: String? {set get}
    var thumbnail: String? {set get}
    var title: String? {set get}
    var overview: String? {set get}
}

class UserCellViewModel: AbstractCellViewModel {
    var id: String?
    var thumbnail: String?
    var title: String?
    var overview: String?
    
    init(id: String? = nil, thumbnail: String? = nil, title: String? = nil, overview: String? = nil) {
        self.id = id
        self.thumbnail = thumbnail
        self.title = title
        self.overview = overview
    }
}


class MessageCellViewModel: AbstractCellViewModel {
    var id: String?
    var thumbnail: String?
    var title: String?
    var overview: String?
    var isIncoming: Bool?
    
    init(id: String? = nil, thumbnail: String? = nil, title: String? = nil, overview: String? = nil, isIncoming: Bool? = nil) {
        self.id = id
        self.thumbnail = thumbnail
        self.title = title
        self.overview = overview
        self.isIncoming = isIncoming
    }
}
