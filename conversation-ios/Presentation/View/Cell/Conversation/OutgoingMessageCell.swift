//
//  OutgoingMessageCell.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/14/22.
//

import UIKit

typealias OutgoingMessageCellConfig = ListViewCellConfigurator<OutgoingMessageCell, AbstractCellViewModel>

class OutgoingMessageCell: UITableViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    // MARK: UI OBJECTS
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(data: DataType) {
        lblName.text = "Me"
        lblMessage.text = data.overview
    }
}
