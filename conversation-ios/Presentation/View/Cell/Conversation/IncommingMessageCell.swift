//
//  IncommingMessageCell.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/14/22.
//

import UIKit

typealias IncommingMessageCellConfig = ListViewCellConfigurator<IncommingMessageCell, AbstractCellViewModel>

class IncommingMessageCell: UITableViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    // MARK: UI Objects
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(data: DataType) {
        lblName.text = data.thumbnail?.capitalized.first?.description ?? "" 
        lblMessage.text = data.overview
    }
    
}
