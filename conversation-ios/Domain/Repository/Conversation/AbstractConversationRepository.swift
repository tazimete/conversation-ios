//
//  AbstractConversationRepository.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation
import RxSwift

/* This is Conversation repository abstraction extented from AbstractRepository. Which will be used to get conversation related from api client/server response*/
protocol AbstractConversationRepository: AbstractRepository {
     func post(message: Message) -> Observable<ConversationApiRequest.ResponseType>
     func get(message: Message) -> Observable<ConversationApiRequest.ResponseType>
}
