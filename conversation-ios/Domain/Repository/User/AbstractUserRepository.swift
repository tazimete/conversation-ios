//
//  AbstractMovieRepository.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation
import RxSwift

/* This is User repository abstraction extented from AbstractRepository. Which will be used to get user related from api client/server response*/
protocol AbstractUserRepository: AbstractRepository {
     func get(limit: Int) -> Observable<UserApiRequest.ResponseType>
}
