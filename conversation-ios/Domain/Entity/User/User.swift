//
//  User.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 31/10/21.
//

import Foundation

/* User entity of api response */
struct User: Codable, Equatable {
    public var id: String?
    public var name: String?
    public var gender: String?
    public var email: String?
    public var message: String?
    public var picture: String?
    
    init(id: String? = nil, name: String? = nil, gender: String? = nil, email: String? = nil, message: String? = nil, picture: String? = nil) {
        self.id = id
        self.name = name
        self.gender = gender
        self.email = email
        self.message = message
        self.picture = picture
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case gender = "gender"
        case email = "email"
        case message = "message"
        case picture = "picture"
    }
    
    public var asCellViewModel: AbstractCellViewModel {
        return UserCellViewModel(id: id, thumbnail: picture, title: name, overview: message)
    }
    
    static func ==(lhs: User, rhs: User) -> Bool {
        return lhs.name == rhs.name && lhs.id == rhs.id
    }
}

