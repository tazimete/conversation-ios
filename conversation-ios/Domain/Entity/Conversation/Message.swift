//
//  Message.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation

/* Message entity of api response */
struct Message: Codable {
    public let id: String?
    public let userToId: String?
    public let userFromId: String?
    public let userToName: String?
    public let userFromName: String?
    public let body: String?
    public let isIncoming: Bool?
    
    init(id: String? = nil, userToId: String? = nil, userFromId: String? = nil, userToName: String? = nil, userFromName: String? = nil, body: String? = nil, isIncoming: Bool? = nil) {
        self.id = id
        self.userToId = userToId
        self.userFromId = userFromId
        self.userToName = userToName
        self.userFromName = userFromName
        self.body = body
        self.isIncoming = isIncoming
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case userToId = "userToId"
        case userFromId = "userFromId"
        case userToName = "userToName"
        case userFromName = "userFromName"
        case body = "body"
        case isIncoming = "isIncoming"
    }
    
    public var asCellViewModel: MessageCellViewModel {
        return MessageCellViewModel(id: id, thumbnail: userFromName, title: userFromName, overview: body, isIncoming: isIncoming)
    }
}
