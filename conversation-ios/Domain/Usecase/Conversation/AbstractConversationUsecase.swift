//
//  AbstractConversationUsecase.swift
//  conversation-ios
//
//  Created by AGM Tazim on 1/16/22.
//

import Foundation
import RxSwift

/* This is Conversation usecase abstraction extented from AbstractUsecase. Which will be used to get conversation related data from conversation repository*/
protocol AbstractConversationUsecase: AbstractUsecase {
    func postMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType>
    func getMessage(message: Message) -> Observable<ConversationApiRequest.ResponseType>
}
