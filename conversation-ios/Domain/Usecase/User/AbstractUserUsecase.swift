//
//  AbstractMovieUsecase.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation
import RxSwift

/* This is User usecase abstraction extented from AbstractUsecase. Which will be used to get user related data from user repository*/
protocol AbstractUserUsecase: AbstractUsecase {
    func getUserList(limit: Int) -> Observable<UserApiRequest.ResponseType>
}
